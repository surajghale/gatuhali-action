import sqlalchemy as sa
from sqlalchemy.dialects import postgresql
from gauthali.database import table
from sqlalchemy import asc
from gauthali.constants import idle_threshold
import pandas as pd
import tempfile
import os
from datetime import datetime, date


def get_value(conn, key):
    reporting_meta = table(conn, "reporting_meta")
    cursor = conn.execute(
        sa.select([reporting_meta.c.value]).where(reporting_meta.c.key == key)
    )
    result = cursor.fetchone()
    if result:
        return result[0]
    return None


def set_value(conn, key, value):
    reporting_meta = table(conn, "reporting_meta")
    insert_stmt = postgresql.insert(reporting_meta).values(key=key, value=value)
    conn.execute(
        insert_stmt.on_conflict_do_update(
            constraint="uq_reporting_meta_key",
            set_=dict(value=insert_stmt.excluded.value),
            where=(reporting_meta.c.key == insert_stmt.excluded.key),
        )
    )


def timestamps(conn):
    from_timestamp = get_value(conn, "last_event")
    depository = table(conn, "depository")
    query = sa.select([depository.c.timestamp])
    if from_timestamp is not None:
        query = query.where(
            sa.and_(
                depository.c.timestamp > from_timestamp,
                depository.c.timestamp <= sa.func.now(),
            )
        ).order_by(asc(depository.c.timestamp))
    else:
        query = query.order_by(depository.c.timestamp).limit(1000)
    df = pd.read_sql_query(query, conn)
    return df.iloc[0]["timestamp"], df.iloc[-1]["timestamp"]


def get_data(conn, member, from_timestamp, to_timestamp):
    depository = table(conn, "depository")
    members = table(conn, "members")
    team_members = table(conn, "team_members")
    tasks = table(conn, "tasks")

    query = (
        sa.select(
            [
                depository.c.member_id,
                depository.c.task_id,
                depository.c.kind,
                depository.c.name,
                depository.c.timestamp,
                depository.c.data,
                members.c.workspace_id,
                team_members.c.team_id,
                tasks.c.project_id,
            ]
        )
        .select_from(
            depository.join(members, depository.c.member_id == members.c.id)
            .join(team_members, members.c.id == team_members.c.member_id)
            .join(tasks, depository.c.task_id == tasks.c.id)
        )
        .where(
            sa.and_(
                depository.c.timestamp > from_timestamp,
                depository.c.timestamp <= to_timestamp,
                depository.c.member_id == member,
                sa.or_(depository.c.kind == "event", depository.c.kind == "action"),
            )
        )
        .order_by(asc(depository.c.timestamp))
    )
    return conn.execute(query).fetchall()


def process_member_events(conn, member, workspace, from_timestamp, to_timestamp):
    events = get_data(conn, member, from_timestamp, to_timestamp)
    if len(events):
        data = event_status(conn, events, member, workspace, idle_threshold)
        if len(data):
            save_to_db(conn, member, data, datetime.utcnow().date())


def save_to_db(conn, member, entries, date):
    tb = table(conn, "members_data")
    record = conn.execute(
        sa.select([tb]).where(
            sa.and_(tb.c.member_id == member, tb.c.recorded_on == date)
        )
    ).fetchone()
    if record:
        old = pickling(record["data"], unpickle=True)
        old.extend(entries)
        query = (
            tb.update()
            .values(
                member_id=member,
                recorded_on=date,
                data=pickling(old, unpickle=False),
            )
            .where(sa.and_(tb.c.member_id == member, tb.c.recorded_on == date))
        )
    else:
        query = tb.insert().values(
            member_id=member,
            recorded_on=date,
            data=pickling(entries, unpickle=False),
        )

    conn.execute(query)


def save_in_disc(member, data):
    temp_path = tempfile.gettempdir()
    f = os.path.join(temp_path, f"{member}.pickle")
    with open(f, "wb") as _file:
        pickled = pickling(data, unpickle=False)
        _file.write(pickled)


def save_csv(data):
    df = pd.DataFrame.from_records(data)
    temp_path = tempfile.gettempdir()
    df.to_csv(os.path.join(temp_path, "data.csv"), index=False, header=True)


def save_all_to_db(conn, members):
    temp_path = tempfile.gettempdir()
    data = []
    for member in members:
        path = os.path._join(temp_path, f"{member}.pickle")
        unpickled = pickling(path, unpickle=True)
        data.append(unpickled)

    tb = table(conn, "hourly_data")
    record = conn.execute(
        sa.select(tb).where(tb.c.timestamps == date.today().isoformat())
    ).fetchone()
    if record:
        old = pickling(record["data"], unpickle=True)
        old.extend(data)
        query = (
            tb.update()
            .values(
                data_date=member,
                data=pickling(old, unpickle=False),
            )
            .where()
        )
    else:
        query = tb.insert().values(
            timestamps=date.today().isoformat(),
            data=pickling(data, unpickle=False),
        )

    conn.execute(query)


def pickling(data, unpickle=False):
    import pickle

    try:
        if unpickle:
            result = pickle.loads(data)
        else:
            result = pickle.dumps(data)
    except pickle.UnpicklingError:
        raise ("cannot unpickle the data")
    except pickle.PicklingError:
        raise ("cannot pickle the data")
    else:
        return result


def get_flag(application, apps, catalog):
    app = removesuffix(application, "-exe")
    if catalog and app in apps:
        app_id = apps[app]
        if app_id in catalog["productive"]:
            return "Productive"
        elif app_id in catalog["unproductive"]:
            return "Unproductive"
        elif app_id in catalog["neutral"]:
            return "Neutral"

    return "Undefined"


def add(member, workspace, task, team, project, app, status, flag, start, end, data):
    data.append(
        {
            "member_id": member,
            "task_id": task,
            "team_id": team,
            "project_id": project,
            "workspace_id": workspace,
            "app": app,
            "date_range": [start, end],
            "time_duration": (end - start).total_seconds(),
            "status": status,
            "flag": flag,
        }
    )


def event_status(conn, events, member, workspace, idle_threshold):
    """
    (Algorithm)

    Pseudocode

    while index less than length(events):
        calculate time difference of consecutive events.
        if event is (clockin,clockout):
            store the clockin, clockout time
        if difference is above certain threshold:
                store the total time duration of the previous app before user goes inactive. i.e. (start_time to x_event_time)
            set status to idle
            if inactive time is (private or break):
                set status to private or break
            endif
            store the inactive (idle, private, break) time duration.(x_event_time to event_time)
            set start_time to current event_time
        endif
        else if consecutive events are from diff app:
            (store the time duration of beginning of previous app
            to end of previous app i.e. (start_time to x_event_time))

            set start_time to current event_time
        endif

        x_app = app
        x_task = task
        x_team = team
        x_project = project
        x_event_time = event_time
        index += 1

    endwhile


    #if all the events are of same app and there is no inactive (idle, private, break) time
    store the time duration from beggining(start_time) to end(x_event_time) of the app

    return stored_value

    """
    _apps = apps(conn)
    catalog = app_catalog(conn, member)

    x_app, x_event_time, x_task, x_team, x_project, x_event_type = extract(events[0])
    start_time = x_event_time
    index = 1
    status_data = []

    while index < len(events):
        app, event_time, task, team, project, event_type = extract(events[index])
        idle = (
            lambda event_time, x_event_time, idle_threshold: (
                event_time - x_event_time
            ).total_seconds()
            > idle_threshold * 60
        )
        if idle:
            status, flag, start, end = get_values(
                x_event_type, start_time, x_event_time, False
            )

            add(
                member=member,
                workspace=workspace,
                data=status_data,
                task=x_task,
                team=x_team,
                project=x_project,
                app=x_app,
                status=status,
                flag=flag if flag else get_flag(x_app, _apps, catalog),
                start=start,
                end=end,
            )
            status, flag, start, end = get_values(
                x_event_type, x_event_time, event_time, True
            )

            add(
                member=member,
                workspace=workspace,
                data=status_data,
                task=x_task,
                team=x_team,
                project=x_project,
                app=x_app,
                status=status,
                flag="Unproductive" if status == "idle" else "neutral",
                start=start,
                end=end,
            )

            start_time = event_time

        elif x_app != app:
            status, flag, start, end = get_values(
                x_event_type, x_event_time, event_time
            )

            add(
                member=member,
                workspace=workspace,
                data=status_data,
                task=x_task,
                team=x_team,
                project=x_project,
                app=x_app,
                status=status,
                flag=flag if flag else get_flag(x_app, _apps, catalog),
                start=start,
                end=end,
            )

            start_time = event_time

        x_app = app
        x_task = task
        x_team = team
        x_project = project
        x_event_time = event_time
        index += 1

    status, flag, start, end = get_values(x_event_type, start_time, x_event_time)
    add(
        member=member,
        workspace=workspace,
        data=status_data,
        task=x_task,
        team=x_team,
        project=x_project,
        app=x_app,
        status=status,
        flag=flag if flag else get_flag(x_app, _apps, catalog),
        start=start,
        end=end,
    )

    return status_data


def get_values(x_event_type, start, end, idle=False):
    if x_event_type in [
        "clock-in",
        "clock-out",
        "break-start",
        "break-stop",
        "private-time-start",
        "private-time-stop",
    ]:
        status = "idle"
        endtime = start
        if idle:
            status = (
                lambda x_event_type, event_type: "break"
                if x_event_type == "break-start" and event_type == "break-stop"
                else (
                    "private"
                    if x_event_type == "private-time-start"
                    and event_type == "private-time-stop"
                    else "offline"
                )
            )
            endtime = end

        return (
            status if idle else x_event_type,
            "Unproductive" if status == "idle" else "neutral",
            start,
            endtime,
        )

    return "online", None, start, end


def extract(row):
    if row["kind"] == "event":
        data = row["data"]
        app = data["application"]
    else:
        app = "name"
    return (
        app,
        row["timestamp"],
        row["task_id"],
        row["team_id"],
        row["project_id"],
        row["name"],
    )


def app_catalog(conn, member):
    workspace_application_catalog = table(conn, "workspace_application_catalog")
    members = table(conn, "members")
    cursor = conn.execute(
        sa.select([workspace_application_catalog])
        .select_from(
            members.join(
                workspace_application_catalog,
                members.c.workspace_id == workspace_application_catalog.c.workspace_id,
            )
        )
        .where(members.c.id == member)
    )
    return cursor.fetchone()


def apps(conn):
    application_catalog = table(conn, "application_catalog")
    return {
        rec["name"]: rec["id"]
        for rec in conn.execute(
            sa.select([application_catalog.c.name, application_catalog.c.id])
        )
    }


def removesuffix(string: str, suffix: str, /) -> str:
    if suffix and string.endswith(suffix):
        return string[: -len(suffix)]
    return string[:]
