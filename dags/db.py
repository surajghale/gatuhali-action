from sqlalchemy import Table, MetaData
from airflow.hooks import postgres_hook
from models import db_conn_id


def engine():
    return postgres_hook.PostgresHook(db_conn_id).get_sqlalchemy_engine()


def table(conn, table):
    return Table(table, MetaData(bind=None), autoload=True, autoload_with=conn)
