import sqlalchemy as sa
from sqlalchemy.dialects import postgresql
from db import table, engine
from datetime import timedelta
import pickle
from datetime import date
import collections
import functools
import psycopg2
import datetime
import operator
import pandas as pd
from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String, PickleType, Date
# engine = create_engine('sqlite:///college.db', echo = True)
meta = MetaData()


def pickling(data, unpickle=False):
    import pickle
    try:
        if unpickle:
            result = pickle.loads(data)
        else:
            result = pickle.dumps(data)
    except pickle.UnpicklingError:
        raise ("cannot unpickle the data")
    except pickle.PicklingError:
        raise ("cannot pickle the data")
    else:
        return result


# def createTable(conn):
#     try:
#         member_data = table(conn, "member_data")
#         appusage_members = table(conn, "appusage_members")
#         appusage_teams = table(conn, "appusage_teams")
#         appusage_projects = table(conn, "appusage_projects")
#         print("try block ----------------------------")
#         return member_data, appusage_members, appusage_teams, appusage_projects
#     except Exception as e:
#         # member usage table creation
#         members_data = sa.Table(
#         "member_data",
#         meta,
#         sa.Column("id", sa.Sequence("members_data_id_seq"), primary_key=True),
#         sa.Column("recorded_on", postgresql.DATE, nullable=False),
#         sa.Column("member_id", postgresql.UUID(as_uuid=True), nullable=False),
#         sa.Column("data", postgresql.BYTEA, nullable=False),
#         # sa.ForeignKeyConstraint(["member_id"], ["members.id"]),
#     )
#         appusage_members = sa.Table(
#         "appusage_members",
#         meta,
#         sa.Column("id", sa.Sequence("appusage_members_id_seq"), primary_key=True),
#         sa.Column("recorded_on", postgresql.DATE, nullable=False),
#         sa.Column("total_usage", sa.FLOAT(), nullable=False),
#         sa.Column("member_id", postgresql.UUID(as_uuid=True), nullable=False),
#         sa.Column("data", postgresql.BYTEA, nullable=False),
#         # sa.ForeignKeyConstraint(["member_id"], ["members.id"]),
#         )
 
#         appusage_teams = sa.Table(
#         "appusage_teams",
#         meta,
#         sa.Column("id", sa.Sequence("appusage_teams_id_seq"), primary_key=True),
#         sa.Column("recorded_on", postgresql.DATE, nullable=False),
#         sa.Column("total_usage", sa.FLOAT(), nullable=False),
#         sa.Column("team_id", postgresql.UUID(as_uuid=True), nullable=False),
#         sa.Column("data", postgresql.BYTEA, nullable=False),
#         # sa.ForeignKeyConstraint(["team_id"], ["teams.id"]),
#     )
#         appusage_projects = sa.Table(
#             "appusage_projects",
#             meta,
#             sa.Column("id", sa.Sequence("appusage_projects_id_seq"), primary_key=True),
#             sa.Column("recorded_on", postgresql.DATE, nullable=False),
#             sa.Column("total_usage", sa.FLOAT(), nullable=False),
#             sa.Column("project_id", postgresql.UUID(as_uuid=True), nullable=False),
#             sa.Column("data", postgresql.BYTEA, nullable=False),
#             # sa.ForeignKeyConstraint(["project_id"], ["projects.id"]),
#         )
#         print("nepal========================")
#         meta.create_all(conn)
#         return members_data, appusage_members, appusage_teams, appusage_projects


# # access events data from table ===================================

# def event_data(extracting_day):
#     member_list = member.select().where(member.c.time_stamp==extracting_day)
#     return member_list

# def app_usage_details(conn):
    
#     members_data, appusage_members, appusage_teams,appusage_projects = createTable(conn)
#     print("table is ========================",



def createTable(conn):
    try:
        appusage_members = table(conn, "appusage_members1")
        appusage_teams = table(conn, "appusage_teams1")
        appusage_projects = table(conn, "appusage_projects1")
        members_data = table(conn,"members_data")
        print("try block ----------------------------")
        return member_usage1, team_usage, project_usage
    except Exception as e:
        # member usage table creation
        appusage_members = Table(
            'appusage_members1', meta,
            Column('id', Integer, primary_key=True),
            Column('member_id', String),
            Column('data', PickleType),
            Column('total_usage', PickleType),
            Column('recorded_on', Date),
            
        )
        meta.create_all(conn)
        # appusage_teams = table(conn, "appusage_teams")
        # team usage table creation
        appusage_teams = Table(
            'appusage_teams1', meta,
            Column('id', Integer, primary_key=True),
            Column('team_id', String),
            Column('data', PickleType),
            Column('total_usage', PickleType),
            Column('recorded_on', Date),
        )
        meta.create_all(conn)
        # team_usage = table(conn, "team_usage")
        # project usage table creation
        appusage_projects = Table(
            'appusage_projects1', meta,
            Column('id', Integer, primary_key=True),
            Column('project_id', String),
            Column('data', PickleType),
            Column('total_usage', PickleType),
            Column('recorded_on', Date),
        )
        meta.create_all(conn)
        # project_usage = table(conn, "project_usage")
        return appusage_members, appusage_teams, appusage_projects

# 
# access events data from table ===================================

def event_data(extracting_day):
    member_list = member.select().where(member.c.time_stamp==extracting_day)
    return member_list

def app_usage_details(conn):
    # appusage_members, appusage_teams, appusage_projects = createTable(conn)
    # testing for connecting to members_data table
    # def retreve_value(conn):
    meta.create_all(conn)
    members = table(conn, "members_data")
    mem = table(conn, "member_usage1")
    appusage_members = table(conn,"appusage_members1")
    appusage_teams = table(conn,"appusage_teams2")
    appusage_projects = table(conn,"appusage_projects1")

    member_list = members.select()
    first_row = conn.execute(member_list)
    all_member = first_row.fetchall()

# -------------------------------------------------------------------
# DATA FORMATION


# +++++++++++++++++++++++++++++++++++   DATA END    -------------------------------------


    # member_list = pickling(first["data"], unpickle=True)
    # print("data is ==========================",appusage_members,appusage_projects,appusage_teams)


# -------------------------------------------------------------------------------------------------

    # MEMBER PART FINISHED ===========================================

    for member in all_member:
        member_data = pickling(member["data"], unpickle=True)
        # print("member-------------------",member_data,type(member_data))
        members = []
        member_df = pd.DataFrame(member_data)
        df1 = member_df.groupby(['member_id'])
        # print("df is -------------",member_df)
        for mem, data in df1:
            # print("member -------------",mem)
            app = data.groupby(['app']).agg(
                {'time_duration': 'sum'}).reset_index()
            app_used = app.set_index('app').T.to_dict(orient="records")

            x = app_used[0]
            productivity = data.groupby(['flag']).agg(
                {'time_duration': 'sum'}).reset_index()
            productivity_total = productivity.set_index(
                'flag').T.to_dict(orient="records")
            y = productivity_total[0]
            # print(y)
            x.update(y)
            single_member = [x]
            # print("single data is =================",single_member)
            # x["member_id"] = imem
            members.append(x)
        # print("membsers are===========",members)
        # INSERT DATA TO TABLE==================================================
            try:
                member_id = appusage_members.select(sa.and_(appusage_members.c.member_id == mem,appusage_members.c.recorded_on==date.today().isoformat()))
                first_row = conn.execute(member_id)
                first = first_row.fetchone()

                member = pickling(first["data"], unpickle=True)
                
                # member_without_id =  member[0]
                # result_member = member_without_id.pop("member_id")
                update_member = member + single_member
                print("update member are ==================",update_member)
            #     # this contains the total app_usage and total productivity of each update on the table
                result = dict(functools.reduce(operator.add,
                                                map(collections.Counter, update_member)))
                result['member_id'] = mem
                print("resultant dictionary : ", [result])
                member_data = appusage_members.update().where(appusage_members.c.member_id == mem).values(
                        member_id = mem,
                        recorded_on = date.today().isoformat(),
                        data=pickling(update_member, unpickle=False),
                        total_usage=pickling(result, unpickle=False),
                    )
                conn.execute(member_data)
            
            except Exception as e:
                print("errorr-------------------------")
                member_data = appusage_members.insert().values(
                    recorded_on=date.today().isoformat(),
                    member_id = mem,
                    data=pickling(single_member, unpickle=False),
                    total_usage=pickling(single_member, unpickle=False),

                )
                conn.execute(member_data)
                # member.append(y)

# # # ----------------------------------------------------------------------------------------------------
# # # BY TEAM APP USAGE FINISHED    ===============================++++++++++++++++++++++++++++++++++++++++++++++++++++
    team_data = []
    for i in all_member:
        team_data.append(pickling(i["data"], unpickle=True))
    flat_list = [flat for i in team_data for flat in i]
    # flat_member_data = pickling(all_member[0]["data"], unpickle=True)
    df = pd.DataFrame(flat_list)
    print("flat list is =-------------------------",flat_list)
    team = df.groupby(['team_id'])
    teams = []
    for t, data in team:
        app = data.groupby(['app']).agg({'time_duration': 'sum'}).reset_index()
        app_used = app.set_index('app').T.to_dict(orient="records")
        x = app_used[0]
        productivity = data.groupby(['flag']).agg(
            {'time_duration': 'sum'}).reset_index()

        productivity_total = productivity.set_index(
            'flag').T.to_dict(orient="records")
        y = productivity_total[0]
        
        x.update(y)
        single_team = [x]
        
        # x["team_id"] = t
        print("team is -------------------",single_team)
        try:
            team_id = appusage_teams.select(sa.and_(appusage_teams.c.team_id == t, appusage_teams.c.recorded_on ==date.today().isoformat()))
            first_row = conn.execute(team_id)
            first = first_row.fetchone()

            team = pickling(first["data"], unpickle=True)
            print("data is ==========================", team)
            update_team = team + single_team
            # this contains the total app_usage and total productivity of each update on the table
            result = dict(functools.reduce(operator.add,
                                           map(collections.Counter, update_team)))

            print("resultant dictionary : ", [result],update_team)
            team_data = appusage_teams.update().where(sa.and_(appusage_teams.c.team_id == t,appusage_teams.c.recorded_on == date.today().isoformat())).values(
                recorded_on=date.today().isoformat(),
                team_id = t,
                data=pickling(update_team, unpickle=False),
                total_usage=pickling(result, unpickle=False),
            )
            conn.execute(team_data)
        except Exception as e:
            print("except part ================================")
            team_data = appusage_teams.insert().values(
                recorded_on = date.today().isoformat(),
                team_id = t,
                data=pickling(single_team, unpickle=False),
                total_usage=pickling(single_team, unpickle=False),

            )
            conn.execute(team_data)
        teams.append(x)


# # # ----------------------------------------------------------------------------------------------------
# # # BY PROJECT APP USAGE===============================++++++++++++++++++++++++++++++++++++++++++++++++++++
    project_data = []
    for i in all_member:
        project_data.append(pickling(i["data"], unpickle=True))
    flat_list_project = [flat for i in project_data for flat in i]
    # flat_member_data = pickling(all_member[0]["data"], unpickle=True)
    df = pd.DataFrame(flat_list_project)
    print("flat list is =-------------------------",flat_list_project)
    project = df.groupby(['project_id'])
    projects = []
    for p, data in project:
        app = data.groupby(['app']).agg({'time_duration': 'sum'}).reset_index()
        app_used = app.set_index('app').T.to_dict(orient="records")
        x = app_used[0]
        productivity = data.groupby(['flag']).agg(
            {'time_duration': 'sum'}).reset_index()

        productivity_total = productivity.set_index(
            'flag').T.to_dict(orient="records")
        y = productivity_total[0]
        
        x.update(y)
        single_project = [x]
        print("project ==============",p)
        # x["project_id"] = p
        try:
            project_id = appusage_projects.select(sa.and_(appusage_projects.c.project_id == p,appusage_projects.c.recorded_on == date.today().isoformat()))
            first_row = conn.execute(project_id)
            first = first_row.fetchone()

            project = pickling(first["data"], unpickle=True)
            print("try  part ==========================")
            update_project = project + single_project
            # this contains the total app_usage and total productivity of each update on the table
            result = dict(functools.reduce(operator.add,
                                            map(collections.Counter, update_project)))

            print("resultant dictionary : ", result,update_project)
            project_data = appusage_projects.update().where(sa.and_(appusage_projects.c.project_id == p,appusage_projects.c.recorded_on == date.today().isoformat())).values(
                recorded_on=date.today().isoformat(),
                project_id = p,
                data=pickling(update_project, unpickle=False),
                total_usage=pickling(result, unpickle=False),
            )
            conn.execute(project_data)
        except Exception as e:
            print("exception part===============")
            project_data = appusage_projects.insert().values(
                recorded_on=date.today().isoformat(),
                project_id = p,
                data=pickling(single_project, unpickle=False),
                total_usage=pickling(single_project, unpickle=False),

            )
            conn.execute(project_data)
        projects.append(x)

# ----------------------------------------------------------------------------------------------------
# TIME AND ATTENDENCE PART ============================================================================

