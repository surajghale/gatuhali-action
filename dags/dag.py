from airflow import DAG
from airflow.utils.dates import days_ago
from airflow.operators.python_operator import PythonOperator
from datetime import timedelta
# import sys
# sys.path.append("..\airflow>")
from app_usage import app_usage_details
# from gauthali.time_and_attendence import attendence_details
from db import engine, table
import sqlalchemy as sa
from datetime import datetime, timedelta
import datetime as dt

default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "start_date": days_ago(2),
    "email": ["airflow@example.com"],
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 1,
    # "retry_delay": timedelta(minutes=5),
}
# RUN ACTION FUNCTION ONLY WHEN THE TRUE/SUCCESS EVENT WAS FIRED FROM SOME DAGS OR UPDATE STATUS WAS FIRED FROM DB
# def after_sucess(status):
#     if status:
#         action(ds,**kwargs)
#         timeAndattendece(ds,**kwargs)
#     else:
#         return "waiting"
# def after_sucess_team(status):
#     if status:
        
#         timeAndattendece(ds,**kwargs)
#     else:
#         return "waiting"
def action(ds, **kwargs):
    # last_timestamp = kwargs["ti"].xcom_pull(key="end_time")
    with engine().connect() as conn:
        app_usage_details(conn)
# def timeAndattendece(ds,**kwargs):
#     with engine().connect() as conn:
#         timeAndattendece(conn)
with DAG('gauthali_dags',
         default_args=default_args,
        #  start_date=days_ago(2),
         schedule_interval=dt.timedelta(seconds=3)
         ) as dag:

    write_to_pg_operator = PythonOperator(task_id='web_usage', python_callable=action, provide_context=True, dag=dag)
    # write_to_pg_operator1 = PythonOperator(task_id='time_attendence', python_callable=after_sucess_team, provide_context=True, dag=dag)
    # write_to_pg_operator >> write_to_pg_operator1
    